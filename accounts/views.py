from django.shortcuts import render, redirect
from accounts.forms import LogInForm, SignUpForm, ContactMeForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.core.mail import send_mail
# from django.template.loader import render_to_string


def user_login(request):
    if request.method == "POST":
        form = LogInForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("mainpage")

    else:
        form = LogInForm()

    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")

#create if username is taken, choose another username
def user_signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
        if password == password_confirmation:
            user = User.objects.create_user(
                username,
                password=password,
            )
            login(request, user)
            return redirect("mainpage")
        else:
            form.add_error("password", "the passwords do not match")
    else:
        form = SignUpForm()

    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)



#Sending an email, email not showing up in mailbox


def email(request):
    form = ContactMeForm()
    if request.method == 'POST':
        form = ContactMeForm(request.POST)
        if form.is_valid():

            # body = {
            name= form.cleaned_data['name']
            content=form.cleaned_data['content']
            email=form.cleaned_data['email']
            date=form.cleaned_data['date']
            # str_date = str(date)
                # 'date': form.cleaned_data['date'],
            # }
            # message = '\n'.join(body.values())
            # sender = form.cleaned_data['email']
            # recipient = ['jackyhousejacky@gmail.com']
            try:
                # send_mail(subject, message, sender, ['jackyhousejacky@gmail.com'], fail_silently=True)
                send_mail(name, content, email, ['jackyhousejacky@gmail.com'])
            except BadHeaderError:
                return HttpResponse("Invalid header found.")
            messages.success(request, "Your response has been submited successfully")
    context = {
        'form':form,
    }
    return render(request, "accounts/email.html", context)
