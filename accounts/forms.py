from django import forms




class LogInForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )

#setting up email and a calendar to pick a tour date from
class DateInput(forms.DateInput):
    input_type = 'date'

class ContactMeForm(forms.Form):
    name = forms.CharField(max_length=200)
    content = forms.CharField(max_length=200)
    email = forms.EmailField()
    date = forms.DateField(widget=DateInput)
