from django.db import models
from django.contrib.auth.models import User





# This class is for people to make a list of reports

ANIMAL_CHOICES = (
    ("CSL", "California Sea Lion"),
    ("GFS", "Guadalupe Fur Seal"),
    ("HS", "Harbor Seal"),
    ("Other", "Other"),
    ("Not Assigned", "Not Assigned"),
    )

class Report(models.Model):
    create_animal_name = models.CharField(max_length=200)
    reporter = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200)
    critical = models.BooleanField(default=False)
    alone = models.BooleanField(default=False)
    malnurished = models.BooleanField(default=False)
    wellnesscheck = models.BooleanField(default=False)
    location = models.CharField(max_length=200)
    created_on = models.DateTimeField(auto_now_add=True)
    notes = models.TextField()
    picture = models.URLField()

    def __str__(self):
        return self.create_animal_name


# This class is for staff to make exams for each report
class Exam(models.Model):
    exam_date = models.DateTimeField(auto_now_add=True)
    specie = models.CharField(max_length=30,
                  choices=ANIMAL_CHOICES,
                  default="Not Assigned")
    size = models.CharField(max_length=200)
    weight = models.CharField(max_length=200)
    age = models.CharField(max_length=200)
    sex = models.CharField(max_length=200)
    bloodworkrequired = models.BooleanField(default=False)
    pen = models.CharField(max_length=200)
    notes = models.TextField()
    medicalpicture = models.URLField()
    medicalreporter = models.ForeignKey(
        User,
        related_name="medical",
        on_delete=models.CASCADE,
        null=True,
    )
    animal = models.ForeignKey(
        Report,
        related_name="exams",
        on_delete=models.CASCADE,
        null=True,
    )


    
