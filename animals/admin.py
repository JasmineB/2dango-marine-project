from django.contrib import admin

from django.contrib import admin
from animals.models import Exam, Report

@admin.register(Exam)
class ExamAdmin(admin.ModelAdmin):
    list_display = (
        "exam_date",
        "specie",
        "size",
        "weight",
        "age",
        "sex",
        "bloodworkrequired",
        "pen",
        "notes",
        "medicalpicture",
        "medicalreporter",
        "id",
    )






@admin.register(Report)
class ReportAdmin(admin.ModelAdmin):
    list_display = (
        "create_animal_name",
        "critical",
        "alone",
        "malnurished",
        "wellnesscheck",
        "location",
        "created_on",
        "notes",
        "picture",
        "reporter",
        "phone_number",
        "id",
    )
