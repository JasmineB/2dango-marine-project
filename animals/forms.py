from django.forms import ModelForm
from animals.models import Exam, Report


class ExamForm(ModelForm):
    class Meta:
        model = Exam
        fields = [
            "specie",
            "size",
            "weight",
            "age",
            "sex",
            "bloodworkrequired",
            "pen",
            "notes",
            "medicalpicture",
            "medicalreporter",
            "animal",

        ]


class ReportForm(ModelForm):
    class Meta:
        model = Report
        fields = [
            "create_animal_name",
            "reporter",
            "phone_number",
            "critical",
            "alone",
            "malnurished",
            "wellnesscheck",
            "location",
            "notes",
            "picture",

        ]
