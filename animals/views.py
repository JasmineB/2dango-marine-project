

from django.shortcuts import render, redirect
from animals.models import Exam, Report
from animals.forms import ExamForm, ReportForm

def show_animal(request, id):
    list = Report.objects.get(id=id)
    context = {
        "list":list,
    }
    return render(request, "animals/detail.html", context)


#This is the list
def animal_list(request):
    lists = Report.objects.all()
    context = {
        "lists": lists,
    }
    return render(request, "animals/health_list.html", context)

def mainpage(request):
    return render(request, "animals/main.html")


def create_animal_exam(request):
    if request.method == "POST":
        form = ExamForm(request.POST)
        if form.is_valid():
            exam=form.save()
            return redirect("show_animal", id=exam.animal.id)
    else:
        form = ExamForm()

    context = {
        "form": form
    }

    return render(request, "animals/create_exam.html", context)


def create_report(request):
    if request.method == "POST":
        form = ReportForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("animal_list")
    else:
        form = ReportForm()

    context = {
        "form": form
    }

    return render(request, "animals/create_report.html", context)


def update_exam(request, id):
    exam = Exam.objects.get(id=id)
    # item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ExamForm(request.POST, instance=exam)
        if form.is_valid():
            exam = form.save()
            return redirect("show_animal", id=exam.animal.id)
    else:
        form = ExamForm(instance=exam)

    context = {

        "form": form,
        }
    return render(request, "animals/edit_exam.html", context)
