from django.urls import path
from animals.views import show_animal, animal_list, mainpage, create_animal_exam, create_report, update_exam

urlpatterns = [
    # path("", show_animal),
    path("<int:id>/", show_animal, name="show_animal"),
    path("health/", animal_list, name="animal_list"),
    path("home/", mainpage, name="mainpage"),
    path("exams/create/", create_animal_exam, name="create_animal_exam"),
    path("reports/create/", create_report, name="create_report"),
    path("exams/<int:id>/update/", update_exam, name="update_exam"),

]
